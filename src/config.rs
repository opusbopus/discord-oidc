use std::{fmt::Debug, net::SocketAddr, str::FromStr};

use tracing::Level;

macro_rules! env {
	($key:literal) => {
		dotenvy::var($key).expect(&format!("env variable '{}' not found", stringify!($key)))
	};
}

pub struct Config {
	pub log_level: Level,
	pub bind_uri: SocketAddr,

	pub discord_guild_id: i64,
	pub discord_required_role_ids: Vec<i64>,
	pub discord_bot_token: String,

	pub redirect_uri: String,

	pub oauth_authorize_uri: String,
	pub oauth_token_uri: String,
	pub oauth_userinfo_uri: String,

	pub oauth_scopes: String,
}

impl Debug for Config {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.debug_struct("Config")
			.field("redirect_uri", &self.redirect_uri)
			.field("oauth_authorize_uri", &self.oauth_authorize_uri)
			.field("oauth_token_uri", &self.oauth_token_uri)
			.field("oauth_userinfo_uri", &self.oauth_userinfo_uri)
			.field("oauth_scopes", &self.oauth_scopes)
			.finish()
	}
}

impl Config {
	pub fn new() -> eyre::Result<Self> {
		Ok(Self {
			log_level: Level::from_str(&env!("LOG_LEVEL"))?,
			bind_uri: env!("BIND_URI").parse()?,

			discord_guild_id: env!("DISCORD_GUILD_ID").parse()?,
			discord_required_role_ids: env!("DISCORD_REQUIRED_ROLE_IDS")
				.split(',')
				.map(|v| v.parse().unwrap())
				.collect(),
			discord_bot_token: env!("DISCORD_BOT_TOKEN"),

			redirect_uri: env!("REDIRECT_URI").parse()?,

			oauth_authorize_uri: env!("OAUTH2_AUTHORIZE_URI"),
			oauth_token_uri: env!("OAUTH2_TOKEN_URI"),
			oauth_userinfo_uri: env!("OAUTH2_USER_INFO_URI"),

			oauth_scopes: env!("OAUTH2_SCOPES"),
		})
	}
}
