FROM docker.io/rust:1.70.0-alpine3.17 as builder

WORKDIR /var/lib/discord-oidc/
COPY . /var/lib/discord-oidc/

RUN apk add --no-cache musl-dev
RUN cargo build --release

FROM docker.io/alpine:3.17

# RUN apk add --no-cache libc6-compat

WORKDIR /app/
COPY --from=builder /var/lib/discord-oidc/target/release/discord-oidc ./
CMD [ "./discord-oidc" ]