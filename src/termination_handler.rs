use std::future::Future;

use tokio::signal::unix::{signal, SignalKind};
use tracing::info;

pub fn termination_handler() -> eyre::Result<impl Future<Output = ()>> {
    let mut interrupt = signal(SignalKind::interrupt())?;
    let mut terminate = signal(SignalKind::terminate())?;

    let future = || async move {
        let interrupt_fut = interrupt.recv();
        let terminate_fut = terminate.recv();

        tokio::pin!(interrupt_fut);
        tokio::pin!(terminate_fut);

        loop {
            tokio::select! {
                _ = interrupt_fut => {
                    break;
                }

                _ = terminate_fut => {
                    break;
                }
            }
        }

        info!("Received sigint/sigterm");
    };

    Ok(future())
}