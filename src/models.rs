use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct DiscordTokenResponse {
	pub access_token: String,
	pub expires_in: i32,
	pub refresh_token: String,
	pub scope: String,
	pub token_type: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DiscordUserInfoResponse {
	pub id: String,
	pub username: String,
	pub global_name: String,
	pub email: String,
	pub verified: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UserInfoResponse {
	pub sub: String,
	pub id: String,
	pub username: String,
	pub email: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TokenForm {
	pub grant_type: String,
	pub redirect_uri: String,
	pub client_id: String,
	pub client_secret: String,
	pub code: String,
}

impl Into<UserInfoResponse> for DiscordUserInfoResponse {
	fn into(self) -> UserInfoResponse {
		UserInfoResponse {
			sub: self.id.to_string(),
			id: self.id,
			username: self.username,
			email: self.email,
		}
	}
}
