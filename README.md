# Discord-OIDC

Wraps Discord's OAuth2 to be used as an OIDC provider.

You can configure it to be used on OAuth other than Discord but don't expect it to work or for me to support it, feel free to make a PR if you'd like <3.