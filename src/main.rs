mod config;
mod models;
mod termination_handler;

use std::{collections::BTreeMap, sync::Arc};

use axum::{
	extract::{Query, State},
	headers::{authorization::Bearer, Authorization},
	response::Redirect,
	routing::{get, post},
	Json, Router, TypedHeader,
};
use eyre::Result;
use reqwest::{Client, StatusCode};
use serde::Deserialize;
use tracing::info;

use crate::{config::Config, models::*};

#[derive(Debug, Deserialize)]
struct GuildMemberResponse {
	roles: Vec<String>,
}

async fn validate_user(config: Arc<Config>, id: i64) -> Result<bool> {
	let client = Client::new();
	let res = client
		.get(format!(
			"https://discord.com/api/guilds/{}/members/{}",
			config.discord_guild_id, id
		))
		.header(
			"Authorization",
			format!("Bot {}", &config.discord_bot_token),
		)
		.send()
		.await?
		.json::<GuildMemberResponse>()
		.await;

	if let Ok(res) = res {
		for id in &config.discord_required_role_ids {
			if res.roles.contains(&id.to_string()) {
				return Ok(true);
			}
		}
	}

	Ok(false)
}

async fn get_user_info(token: &str, uri: &str) -> Result<DiscordUserInfoResponse> {
	let client = Client::new();
	Ok(client
		.get(uri)
		.header("Authorization", format!("Bearer {}", token))
		.send()
		.await?
		.json::<DiscordUserInfoResponse>()
		.await?)
}

#[axum::debug_handler]
async fn auth(
	params: Query<BTreeMap<String, String>>,
	State(config): State<Arc<Config>>,
) -> Result<Redirect, String> {
	let params = params.0;

	let client_id = params
		.get("client_id")
		.ok_or_else(|| String::from("missing client id"))?;
	let redirect_uri = params
		.get("redirect_uri")
		.ok_or_else(|| String::from("missing redirect_uri"))?;
	if redirect_uri != &config.redirect_uri {
		return Err("bad redirect uri".into());
	}
	let state = params
		.get("state")
		.ok_or_else(|| String::from("missing state"))?;

	let params = &[
		("client_id", client_id.as_str()),
		("redirect_uri", redirect_uri),
		("response_type", "code"),
		("scope", &config.oauth_scopes),
		("state", &state),
	];
	let params = serde_urlencoded::to_string(params).map_err(|v| v.to_string())?;
	Ok(Redirect::temporary(&format!(
		"{}?{}",
		config.oauth_authorize_uri, &params
	)))
}

#[axum::debug_handler]
async fn token(
	State(config): State<Arc<Config>>,
	body: String,
) -> Result<Json<DiscordTokenResponse>, (StatusCode, String)> {
	let form: TokenForm =
		serde_urlencoded::from_str(&body).map_err(|v| (StatusCode::BAD_REQUEST, v.to_string()))?;

	let client = Client::new();
	let token_req = client
		.post(&config.oauth_token_uri)
		.form(&[
			("client_id", &form.client_id),
			("client_secret", &form.client_secret),
			("redirect_uri", &form.redirect_uri),
			("code", &form.code),
			("grant_type", &form.grant_type),
			("scope", &config.oauth_scopes),
		])
		.send()
		.await
		.map_err(|v| (StatusCode::BAD_REQUEST, v.to_string()))?;

	let token_res = token_req
		.json::<DiscordTokenResponse>()
		.await
		.map_err(|v| (StatusCode::BAD_REQUEST, v.to_string()))?;

	let token = token_res.access_token.clone();
	let user_info = get_user_info(&token, &config.oauth_userinfo_uri)
		.await
		.map_err(|v| (StatusCode::BAD_REQUEST, v.to_string()))?;

	if !validate_user(config, user_info.id.parse().expect("wat"))
		.await
		.map_err(|v| (StatusCode::BAD_REQUEST, v.to_string()))?
	{
		return Err((StatusCode::UNAUTHORIZED, "bad".into()));
	}

	Ok(Json(token_res))
}

#[axum::debug_handler]
async fn userinfo(
	TypedHeader(Authorization(token)): TypedHeader<Authorization<Bearer>>,
	State(config): State<Arc<Config>>,
) -> Result<Json<UserInfoResponse>, (StatusCode, String)> {
	let user_info = get_user_info(token.token(), &config.oauth_userinfo_uri)
		.await
		.map_err(|v| (StatusCode::BAD_REQUEST, v.to_string()))?;

	if !validate_user(config, user_info.id.parse().expect("wat"))
		.await
		.map_err(|v| (StatusCode::BAD_REQUEST, v.to_string()))?
	{
		return Err((StatusCode::UNAUTHORIZED, "bad".into()));
	}

	Ok(Json(user_info.into()))
}

async fn index() -> String {
	"trans rights ✌️🏳️‍⚧️✌️🏳️‍⚧️✌️🏳️‍⚧️✌️🏳️‍⚧️".into()
}

#[tokio::main]
async fn main() -> Result<()> {
	if cfg!(debug_assertions) {
		dotenvy::dotenv()?;
	}

	color_eyre::install()?;

	let config = Config::new()?;
	let config = Arc::new(config);

	tracing_subscriber::fmt()
		.without_time()
		.with_max_level(config.log_level)
		.init();

	info!("DiscordOIDC Listening @ http://{}", &config.bind_uri);

	let router = Router::new()
		.route("/", get(index))
		.route("/auth", get(auth))
		.route("/token", post(token))
		.route("/userinfo", get(userinfo))
		.with_state(config.clone());
	axum::Server::bind(&config.bind_uri)
		.serve(router.into_make_service())
		.with_graceful_shutdown(termination_handler::termination_handler()?)
		.await?;

	Ok(())
}
